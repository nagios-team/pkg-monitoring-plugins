From 88d991773b6d8ac416ad2931ea8debc949555c7a Mon Sep 17 00:00:00 2001
From: William <william@blackhats.net.au>
Date: Wed, 6 Nov 2024 14:00:23 +1000
Subject: [PATCH] Add dontfrag/random for fping

Support the dont fragment and randomise packet data options for check_fping
---
 plugins/check_fping.c | 19 +++++++++++++++++++
 1 file changed, 19 insertions(+)

--- a/plugins/check_fping.c
+++ b/plugins/check_fping.c
@@ -7,7 +7,7 @@
 * 
 * Description:
 * 
-* This file contains the check_disk plugin
+* This file contains the check_fping plugin
 * 
 * This plugin will use the fping command to ping the specified host for a
 * fast check
@@ -60,6 +60,8 @@
 int target_timeout = 0;
 int packet_interval = 0;
 bool verbose = false;
+bool dontfrag = false;
+bool randomize_packet_data = false;
 int cpl;
 int wpl;
 double crta;
@@ -105,6 +107,10 @@
     xasprintf(&option_string, "%s-S %s ", option_string, sourceip);
   if (sourceif)
     xasprintf(&option_string, "%s-I %s ", option_string, sourceif);
+  if (dontfrag)
+    xasprintf(&option_string, "%s-M ", option_string);
+  if (randomize_packet_data)
+    xasprintf(&option_string, "%s-R ", option_string);
 
 #ifdef PATH_TO_FPING6
   if (address_family != AF_INET && is_inet6_addr(server))
@@ -303,6 +309,8 @@
     {"help", no_argument, 0, 'h'},
     {"use-ipv4", no_argument, 0, '4'},
     {"use-ipv6", no_argument, 0, '6'},
+    {"dontfrag", no_argument, 0, 'M'},
+    {"random", no_argument, 0, 'R'},
     {0, 0, 0, 0}
   };
 
@@ -320,7 +328,7 @@
   }
 
   while (1) {
-    c = getopt_long (argc, argv, "+hVvaH:S:c:w:b:n:T:i:I:46", longopts, &option);
+    c = getopt_long (argc, argv, "+hVvaH:S:c:w:b:n:T:i:I:M:R:46", longopts, &option);
 
     if (c == -1 || c == EOF || c == 1)
       break;
@@ -415,6 +423,12 @@
       else
         usage (_("Interval must be a positive integer"));
       break;
+    case 'R':
+      randomize_packet_data = true;
+      break;
+    case 'M':
+      dontfrag = true;
+      break;
     }
   }
 
@@ -506,6 +520,10 @@
   printf ("    %s\n", _("name or IP Address of sourceip"));
   printf (" %s\n", "-I, --sourceif=IF");
   printf ("    %s\n", _("source interface name"));
+  printf(" %s\n", "-M, --dontfrag");
+  printf("    %s\n", _("set the Don't Fragment flag"));
+  printf(" %s\n", "-R, --random");
+  printf("    %s\n", _("random packet data (to foil link data compression)"));
   printf (UT_VERBOSE);
   printf ("\n");
   printf (" %s\n", _("THRESHOLD is <rta>,<pl>%% where <rta> is the round trip average travel time (ms)"));
